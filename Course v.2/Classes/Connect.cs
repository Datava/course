﻿using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Course_v._2
{
    public class Connect
    {
		private static string connectInfo = ConfigurationManager.ConnectionStrings["ConnToDB"].ConnectionString;
		public static MySqlConnection connection = new MySqlConnection(connectInfo);

		// проверка, существует ли уже такой логин
		public static async Task<bool> CheckLogin(string login) {
			if (connection.State == System.Data.ConnectionState.Closed)
				await connection.OpenAsync();
			string sql = $"SELECT password FROM Users WHERE login = '{login}'";
			MySqlCommand command = new MySqlCommand(sql, connection);
			try {
				object x = await command.ExecuteScalarAsync();
				if (x == null)
					return false;
				else
					return true;
			} catch {
				return true;
			}
		}

		// проверка, существует ли уже такой имеил
		public static async Task<bool> CheckEmail(string email) {
			if (connection.State == System.Data.ConnectionState.Closed)
				await connection.OpenAsync();
			string sql = $"SELECT password FROM Users WHERE email = '{email}'";
			MySqlCommand command = new MySqlCommand(sql, connection);
			try {
				object x = await command.ExecuteScalarAsync();
				if (x == null)
					return false;
				else
					return true;
			} catch {
				return true;
			}
		}
	}
}
