﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Course_v._2
{
	public class Contacts : User
	{
		public int id;
		public int oldCount;
		public List<MyMessage> messages = new List<MyMessage>();
		public DateTime timeLastMessage = new DateTime();

		public Contacts(int id, MySqlConnection connection) {
			this.id = id;
			MySqlDataReader reader2 = null;
			try {
				MySqlCommand secMySqlCommand = new MySqlCommand($"SELECT * FROM `Users` WHERE `id` = '{id}'", Connect.connection);
				if (connection.State == System.Data.ConnectionState.Closed) {
					connection.Open();
				}
				reader2 = (MySqlDataReader)secMySqlCommand.ExecuteReader();
				while (reader2.Read()) {
					string Login = Convert.ToString(reader2["login"]);
					string Name = Convert.ToString(reader2["name"]);
					string Email = Convert.ToString(reader2["email"]);
					DateTime Birthday = Convert.ToDateTime(reader2["birthday"]);
					byte[] Img = (byte[])(reader2["image"]);
					bool IsMale = Convert.ToBoolean(reader2["ismale"]);
					string Surname = Convert.ToString(reader2["surname"]);
					login = Login;
					name = Name;
					surname = Surname;
					image = Img;
					birthday = Birthday;
					isMale = IsMale;
					email = Email;
				}
			} catch (Exception ex) {
				MessageBox.Show(ex.Message + "   10");
			} finally {
				reader2.Close();
			}
		}

		public int GetId() => id;

		public Contacts(int id, string login, string name, string email, DateTime birthday, byte[] image, bool isMale, string surname = null) :base(login, name, email, birthday, image, isMale, surname) {
			this.id = id;
		}

	}
}
