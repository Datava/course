﻿using System;

namespace Course_v._2
{
	public class MyMessage
	{
		public string message;
		public DateTime dateTime;
		public bool IsOutbox;
		public bool isChecked;

		public MyMessage(string message, DateTime dateTime, bool isChecked = true, bool isOutBox = true) {
			this.message = message;
			this.dateTime = dateTime;
			this.IsOutbox = isOutBox;
			this.isChecked = isChecked;
		}
	}
}
