﻿using System;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Course_v._2
{
	public class Client
	{
		public static Contacts currentContact;
		public static int id;
		public static string password = "";
		public static User user = new User();

		public static async Task<bool> Registration() {
			try {
				if (Connect.connection.State == System.Data.ConnectionState.Closed)
					await Connect.connection.OpenAsync();
				string sql = $"INSERT INTO `Users` (`id`, `login`, `email`, `password`, `name`, `surname`, `birthday`, `image`, `ismale`) VALUES (NULL, '{user.login}', '{user.email}', '{password}', '{user.name}', '{user.surname}', @Date, @UserImage, '{(user.isMale ? 1 : 0)}')";
				var command = new MySqlCommand(sql, Connect.connection);
				MySqlParameter blob = new MySqlParameter("@UserImage", MySqlDbType.MediumBlob, user.image.Length);
				MySqlParameter date = new MySqlParameter("@Date", MySqlDbType.Date);
				date.Value = user.birthday.ToString("yyyy-MM-dd");
				blob.Value = user.image;
				command.Parameters.Add(date);
				command.Parameters.Add(blob);
				await command.ExecuteNonQueryAsync();
				return true;
			} catch{
				return false;
			}
		}

		public static async Task SetId(string login) {
			if (Connect.connection.State == System.Data.ConnectionState.Closed)
				await Connect.connection.OpenAsync();
			string sql = $"SELECT id FROM Users WHERE login = '{login}'";
			MySqlCommand command = new MySqlCommand(sql, Connect.connection);
			try {
				object x = await command.ExecuteScalarAsync();
				id = Convert.ToInt32(x);
			} catch { }
		}

		// для авторизации. возвращает тру, если пароль подошел и фолс, если нет
		public static async Task<bool> Authorization(string login) {
			if (Connect.connection.State == System.Data.ConnectionState.Closed)
				await Connect.connection.OpenAsync();
			string sql = $"SELECT password FROM Users WHERE login = '{login}'";
			MySqlCommand command = new MySqlCommand(sql, Connect.connection);
			try {
				object x = await command.ExecuteScalarAsync();
				string truePassword = x.ToString();
				if (truePassword == password) {
					user.login = login;
					await SetId(login);
					return true;
				} else
					return false;
			} catch {
				return false;
			}
		}
	}
}
