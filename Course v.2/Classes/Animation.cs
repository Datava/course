﻿using System;
using System.Windows.Threading;
using System.Windows;

namespace Course_v._2
{
    class Animation
    {
        private DispatcherTimer timerFade, timerSwap, timerEmergence; // таймеры
        private UIElement swapElement1, swapElement2, fadeElement, emergencyElement; // элементы
        protected int milliseconds; // длительность тика
        protected double changeStep; // шаг анимации
		
        // констуктор. (можно поменять длительность тика и шаг анимации)
        public Animation(int milliseconds = 1, double changeStep = 0.1) {
            this.milliseconds = milliseconds;
            this.changeStep = changeStep;

            timerFade = new DispatcherTimer();
            timerFade.Tick += new EventHandler(TimerFade_Tick);
            timerFade.Interval = new TimeSpan(0, 0, 0, 0, milliseconds);
            timerSwap = new DispatcherTimer();
            timerSwap.Tick += new EventHandler(TimerSwap_Tick);
            timerSwap.Interval = new TimeSpan(0, 0, 0, 0, milliseconds);
            timerEmergence = new DispatcherTimer();
            timerEmergence.Tick += new EventHandler(TimerEmergence_Tick);
            timerEmergence.Interval = new TimeSpan(0, 0, 0, 0, milliseconds);
        }
		
        // возвращает время затрачиваемое одной анимацией
        public int GetTime() => (int)((1 / changeStep) * milliseconds);
		
        // приниматели элементов. если запущена другая анимация, новая заканчивается преждевременно
        public void Swap(UIElement element1, UIElement element2) {
            if (timerEmergence.IsEnabled == false && timerFade.IsEnabled == false && timerSwap.IsEnabled == false) {
                swapElement1 = element1;
                swapElement2 = element2;
                swapElement2.Visibility = Visibility.Visible;
                swapElement2.Opacity = 0;
                timerSwap.Start();
            } else {
                element2.Opacity = 1;
                element1.Visibility = Visibility.Hidden;
                element2.Visibility = Visibility.Visible;
            }
        }
        public void Fade(UIElement element) {
            if (timerEmergence.IsEnabled == false && timerFade.IsEnabled == false && timerSwap.IsEnabled == false) {
                fadeElement = element;
                fadeElement.Opacity = 1;
                timerFade.Start();
            } else
                element.Visibility = Visibility.Hidden;
        }
        public void Emergence(UIElement element) {
            if (timerEmergence.IsEnabled == false && timerFade.IsEnabled == false && timerSwap.IsEnabled == false){ 
                emergencyElement = element;
                emergencyElement.Opacity = 0;
                emergencyElement.Visibility = Visibility.Visible;
                timerEmergence.Start();
            } else {
                element.Opacity = 1;
                element.Visibility = Visibility.Visible;
            }
        }
		
        // таймеры исполняющие анимации
        private void TimerSwap_Tick(object sender, EventArgs e) {
            if (swapElement1.Opacity > 0.0)
                swapElement1.Opacity -= changeStep;
            else {
                if (swapElement2.Opacity < 1)
                    swapElement2.Opacity += changeStep;
                else {
                    swapElement2.Opacity = 1;
                    swapElement1.Visibility = Visibility.Hidden;
                    timerSwap.Stop();
                }

            }
        }
        private void TimerFade_Tick(object sender, EventArgs e) {
            if (fadeElement.Opacity > 0.0)
                fadeElement.Opacity -= changeStep;
            else {
                fadeElement.Visibility = Visibility.Hidden;
                timerFade.Stop();
            }
        }
        private void TimerEmergence_Tick(object sender, EventArgs e) {
            if (emergencyElement.Opacity < 1)
                emergencyElement.Opacity += changeStep;
            else {
                emergencyElement.Opacity = 1;
                timerEmergence.Stop();
            }
        }

    }
}

