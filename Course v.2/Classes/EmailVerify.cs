﻿using System;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Configuration;

namespace Course_v._2
{
	class EmailVerify
	{
		private static Random rnd = new Random();
		protected static int code = -1;
		public static void SendVerifyEmail() {
			code = rnd.Next(100000, 999999);
			string s = System.IO.File.ReadAllText(@"K:\111.txt", Encoding.Default).Replace("\n", " ").Replace("llocode", code.ToString());
			MailAddress fromAddress = new MailAddress("llo.on.verify@gmail.com", "llo.on");
			MailAddress toAddress = new MailAddress(Client.user.email);
			using (MailMessage mailMessage = new MailMessage(fromAddress, toAddress))
			using (SmtpClient smtpClient = new SmtpClient()) {
				mailMessage.Subject = "Підтвердіть ваш акаунт";
				mailMessage.IsBodyHtml = true;
				mailMessage.Body = s;
				smtpClient.Host = "smtp.gmail.com";
				smtpClient.Port = 587;
				smtpClient.EnableSsl = true;
				smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
				smtpClient.UseDefaultCredentials = false;
				smtpClient.Credentials = new NetworkCredential(fromAddress.Address, ConfigurationManager.ConnectionStrings["EmailPassword"].ConnectionString);
				smtpClient.Send(mailMessage);
			}
		}

		public static int GetCode() => code;
	}
}
