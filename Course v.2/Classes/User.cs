﻿using System;

namespace Course_v._2
{
    public class User
    {
        public string login;
		public string name;
		public string surname;
		public string email;
		public DateTime birthday;
		public byte[] image;
		public bool isMale;

		public User(string login, string name, string email, DateTime birthday, byte[] image, bool isMale, string surname = null) {
			this.login = login;
			this.name = name;
			this.surname = surname;
			this.email = email;
			this.birthday = birthday;
			this.isMale = isMale;
			this.image = image;
		}
		public User() { }
    }
}
