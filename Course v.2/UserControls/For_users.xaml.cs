﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Course_v._2.UserControls
{
    /// <summary>
    /// Логика взаимодействия для Users.xaml
    /// </summary>
    public partial class Users : System.Windows.Controls.UserControl
	{
        public Users()
        {
            InitializeComponent();
        }

		public void SetImage(byte[] data) {
			BitmapSource bitmap = (BitmapSource)new ImageSourceConverter().ConvertFrom(data);
			if (bitmap.Width > bitmap.Height)
				bitmap = new CroppedBitmap(bitmap, new Int32Rect((int)(bitmap.Width / 2 - bitmap.Height / 2), 0, (int)bitmap.Height, (int)bitmap.Height));
			else
				bitmap = new CroppedBitmap(bitmap, new Int32Rect(0, (int)(bitmap.Height / 2 - bitmap.Width / 2), (int)bitmap.Width, (int)bitmap.Width));
			image_user.Source = bitmap;
		}
		public void SetName(string name) {
			label_name.Content = name;
		}
		public void SetMessage(string message) {
			label_message.Content = message;
		}

		private void button_Open_Click(object sender, RoutedEventArgs e) {

		}
	}
}
