﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Course_v._2.UserControls
{
    /// <summary>
    /// Логика взаимодействия для For_recovery.xaml
    /// </summary>
    public partial class For_recovery :UserControl
    {
        Animation animation = new Animation();

        public For_recovery() {
            InitializeComponent();
        }

        //переход по энтеру
        private void TextBox_email_KeyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.Enter)
                Button_send.Focus();
        }
        private void Button_send_KeyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.Enter)
                Button_send.Focus();
        }
        private void TextBox_login_KeyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.Enter)
                Button_send.Focus();
        }

        // работа подсказок на текстбоксах
        private void TextBox_email_GotFocus(object sender, RoutedEventArgs e) {
            animation.Fade(label_email);
        }
        private void TextBox_email_LostFocus(object sender, RoutedEventArgs e) {
            if (TextBox_email.Text == "")
                animation.Emergence(label_email);
        }
        private void TextBox_code_GotFocus(object sender, RoutedEventArgs e) {
            animation.Fade(label_code);
        }
        private void TextBox_code_LostFocus(object sender, RoutedEventArgs e) {
            if (TextBox_code.Text == "")
                animation.Emergence(label_code);
        }

		private void Button_send_Click(object sender, RoutedEventArgs e) {
			EmailVerify.SendVerifyEmail();
		}
	}
}
