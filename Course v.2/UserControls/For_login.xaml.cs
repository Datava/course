﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Course_v._2.UserControls
{
    public partial class For_login :UserControl
    {
        Animation animation = new Animation();

		public For_login() {
			InitializeComponent();
		}

        // переход по энтеру
        private void TextBox_login_KeyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.Enter)
                TextBox_password.Focus();
        }
        private void TextBox_password_KeyDown(object sender, KeyEventArgs e) {
            if (e.Key == Key.Enter)
                Button_login.Focus();
        }

        // работа подсказок на текстбоксах
        private void TextBox_login_LostFocus(object sender, RoutedEventArgs e) {
			if (TextBox_login.Text == "")
				animation.Emergence(label_login);
		}
        private void TextBox_password_LostFocus(object sender, RoutedEventArgs e) {
            if (TextBox_password.Text == "")
                animation.Emergence(label_password);
        }
		private void TextBox_login_GotFocus(object sender, RoutedEventArgs e) => label_login.Visibility = Visibility.Hidden;
		private void TextBox_password_GotFocus(object sender, RoutedEventArgs e) => label_password.Visibility = Visibility.Hidden;

		private async void Button_login_Click(object sender, RoutedEventArgs e) {
			Client.password = TextBox_password.Text;
			if (await Client.Authorization(TextBox_login.Text)) {
				llo window = new llo();
				window.Show();
				start_window.isClosing = true;
			} else
				System.Windows.Forms.MessageBox.Show("Invalid login or password!");
        }

        private void Button_badMemory_Click(object sender, RoutedEventArgs e) {
        }
		private void Button_signup_Click(object sender, RoutedEventArgs e) {
		}
	}
}
