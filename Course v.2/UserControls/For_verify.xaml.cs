﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Course_v._2
{
    /// <summary>
    /// Логика взаимодействия для For_verify.xaml
    /// </summary>
    public partial class For_verify : UserControl
    {
		Animation animation = new Animation();

		public For_verify() {
			InitializeComponent();
		}

	

		private async void Button_confirm_Click(object sender, RoutedEventArgs e) {
			if (TextBox_code.Text == EmailVerify.GetCode().ToString()) {
				while (!await Client.Registration())
					;
				await Client.SetId(Client.user.login);
				llo window = new llo();
				window.Show();
				start_window.isClosing = true;
			} else
				MessageBox.Show("Invalid code!");
		}

		// работа подсказок на текстбоксах
		private void TextBox_code_GotFocus(object sender, RoutedEventArgs e) {
			animation.Fade(label_code);
		}
		private void TextBox_code_LostFocus(object sender, RoutedEventArgs e) {
			if (TextBox_code.Text == "")
				animation.Emergence(label_code);
		}
		//переход по энтеру
		private void TextBox_code_KeyDown(object sender, KeyEventArgs e) {
			if (e.Key == Key.Enter)
				Button_confirm.Focus();
		}
	}
}
