﻿using System;
using System.Windows.Controls;

namespace Course_v._2.UserControls
{
	/// <summary>
	/// Логика взаимодействия для TopDate.xaml
	/// </summary>
	public partial class TopDate : UserControl
	{
		public TopDate() {
			InitializeComponent();
		}
		public void SetDate(DateTime date) {
			this.date.Content = date.ToString("dd.MM.yyyy");
		}
	}
}
