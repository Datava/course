﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using System.IO;
using System.Text.RegularExpressions;

namespace Course_v._2.UserControls
{
    /// <summary>
    /// Логика взаимодействия для For_signup.xaml
    /// </summary>
    public partial class For_signup :System.Windows.Controls.UserControl
    {
		Animation animation = new Animation();
		System.Drawing.Image img;

        public For_signup() {
            InitializeComponent();
			rButton_male.IsChecked = true;  
		}
		
        private void button_openImage_Click(object sender, RoutedEventArgs e) {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Image files (*.JPG, *.GIF, *.PNG)|*.jpg;*.gif;*.png";
            
            if (openDialog.ShowDialog() == DialogResult.OK) {
                this.Focusable = false;

                img = System.Drawing.Image.FromStream(openDialog.OpenFile());

				BitmapSource bitmap = new BitmapImage(new Uri(openDialog.FileName));
				if (bitmap.Width > bitmap.Height)
					bitmap = new CroppedBitmap(bitmap, new Int32Rect((int)(bitmap.Width / 2 - bitmap.Height / 2), 0, (int)bitmap.Height, (int)bitmap.Height));
				else
					bitmap = new CroppedBitmap(bitmap, new Int32Rect(0, (int)(bitmap.Height / 2 - bitmap.Width / 2), (int)bitmap.Width, (int)bitmap.Width));

				image_user.Source = bitmap;
				EllipseGeometry clip = new EllipseGeometry(new Point(64,64), 64, 64);
				image_user.Clip = clip;
			}
        }

        public byte[] imageToByte(System.Drawing.Image img) {
            using (var ms = new MemoryStream()) {
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                return ms.ToArray();
            }
        }

		private void Button_signup_Click(object sender, RoutedEventArgs e) {
			if (CheckInvalid()) {
				Client.password = TextBox_password.Text;
				Client.user = new User(TextBox_login.Text, TextBox_name.Text, TextBox_email.Text, (DateTime)DatePick.SelectedDate.Value, imageToByte(img), ((bool)rButton_male.IsChecked ? true : false), TextBox_surname.Text);
			}
		}

		//динамическая проверка совпадений логина и имейл
		private async void TextBox_login_TextChanged(object sender, TextChangedEventArgs e) {
			if (await Connect.CheckLogin(TextBox_login.Text))
				TextBox_login.Foreground = Brushes.Red;
			else
				TextBox_login.Foreground = Brushes.Black;
		}
		private async void TextBox_email_TextChanged(object sender, TextChangedEventArgs e) {
			if (await Connect.CheckEmail(TextBox_email.Text))
				TextBox_email.Foreground = Brushes.Red;
			else
				TextBox_email.Foreground = Brushes.Black;
		}

		public bool CheckInvalid() {
			Regex mail = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
			Regex login = new Regex(@"^([a-z]+([-_]*?[a-z0-9]+){0,2}){4,}$", RegexOptions.IgnoreCase);
			Regex name = new Regex(@"^[Є-ЯҐ,A-Z][а-їґ,a-z\']{3,}$");
			Regex pass = new Regex(@"^[0-9a-zA-Z!@#$%^&*]{6,}$");

			Match mMail = mail.Match(TextBox_email.Text);
			Match mLogin = login.Match(TextBox_login.Text);
			Match mName = name.Match(TextBox_name.Text);
			Match mSurname = name.Match(TextBox_surname.Text);
			Match mPass = pass.Match(TextBox_password.Text);

			

			if(!mMail.Success)
				TextBox_email.Background = Brushes.PaleVioletRed;
			else
				TextBox_email.Background = Brushes.White;
			if (!mLogin.Success)
				TextBox_login.Background = Brushes.PaleVioletRed;
			else
				TextBox_login.Background = Brushes.White;
			if (!mName.Success)
				TextBox_name.Background = Brushes.PaleVioletRed;
			else
				TextBox_name.Background = Brushes.White;
			if (!mSurname.Success)
				TextBox_surname.Background = Brushes.PaleVioletRed;
			else
				TextBox_surname.Background = Brushes.White;
			if (!mPass.Success)
				TextBox_password.Background = Brushes.PaleVioletRed;
			else
				TextBox_password.Background = Brushes.White;

			if (TextBox_email.Foreground == Brushes.Red || !mMail.Success) {
				check_image.Visibility = Visibility.Visible;
				return false;
			}
			if (!mName.Success) {
				check_image.Visibility = Visibility.Visible;
				return false;
			}
			if (!mSurname.Success) {
				check_image.Visibility = Visibility.Visible;
				return false;
			}
			if (TextBox_login.Foreground == Brushes.Red || !mLogin.Success) {
				check_image.Visibility = Visibility.Visible;
				return false;
			}
			if (!mPass.Success) {
				check_image.Visibility = Visibility.Visible;
				return false;
			}
			if(image_user.Source == null) {
				check_image.Visibility = Visibility.Visible;
				return false;
			}

			check_image.Visibility = Visibility.Hidden;
			return true;
		}

		// работа подсказок на текстбоксах
		private void TextBox_name_GotFocus(object sender, RoutedEventArgs e) => label_name.Visibility = Visibility.Hidden;
		private void TextBox_surname_GotFocus(object sender, RoutedEventArgs e) => label_surname.Visibility = Visibility.Hidden;
		private void TextBox_login_GotFocus(object sender, RoutedEventArgs e) => label_login.Visibility = Visibility.Hidden;
		private void TextBox_email_GotFocus(object sender, RoutedEventArgs e) => label_email.Visibility = Visibility.Hidden;
		private void TextBox_password_GotFocus(object sender, RoutedEventArgs e) => label_password.Visibility = Visibility.Hidden;
		private void TextBox_name_LostFocus(object sender, RoutedEventArgs e) {
			if (TextBox_name.Text == "")
				animation.Emergence(label_name);
		}
		private void TextBox_surname_LostFocus(object sender, RoutedEventArgs e) {
			if (TextBox_surname.Text == "")
				animation.Emergence(label_surname);
		}
		private void TextBox_login_LostFocus(object sender, RoutedEventArgs e) {
			if (TextBox_login.Text == "")
				animation.Emergence(label_login);
		}
		private void TextBox_email_LostFocus(object sender, RoutedEventArgs e) {
			if (TextBox_email.Text == "")
				animation.Emergence(label_email);
		}
		private void TextBox_password_LostFocus(object sender, RoutedEventArgs e) {
			if (TextBox_password.Text == "")
				animation.Emergence(label_password);
		}

		// работа ентеров
		private void TextBox_name_KeyDown(object sender, System.Windows.Input.KeyEventArgs e) {
			if (e.Key == Key.Enter)
				TextBox_surname.Focus();
		}
		private void TextBox_surname_KeyDown(object sender, System.Windows.Input.KeyEventArgs e) {
			if (e.Key == Key.Enter)
				TextBox_login.Focus();
		}
		private void TextBox_login_KeyDown(object sender, System.Windows.Input.KeyEventArgs e) {
			if (e.Key == Key.Enter)
				TextBox_email.Focus();
		}
		private void TextBox_email_KeyDown(object sender, System.Windows.Input.KeyEventArgs e) {
			if (e.Key == Key.Enter)
				TextBox_password.Focus();
		}
		private void TextBox_password_KeyDown(object sender, System.Windows.Input.KeyEventArgs e) {
			if (e.Key == Key.Enter)
				DatePick.Focus();
		}
		private void button_openImage_GotFocus(object sender, RoutedEventArgs e) => button_focus.Focus();
	}
}