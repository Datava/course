﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Course_v._2.UserControls
{
    /// <summary>
    /// Логика взаимодействия для For_messages.xaml
    /// </summary>
    public partial class For_messages : UserControl
    {
        public For_messages()
        {
            InitializeComponent();
        }

		public void SetMessage(DateTime dt, string mes, bool isOutbox = true) {
			if (isOutbox)
				textBlock_m.Text = mes;
			else { 
				label_message.Visibility = Visibility.Hidden;
				textBlock_m_Copy.Text = mes;
				label_message_Copy.Visibility = Visibility.Visible;
			}
				textBlock_date.Text = dt.ToString("hh:mm:ss");
			this.Height = textBlock_m.Height + 20;
			grid.Height = textBlock_m.Height + 20;
		}
    }
}
