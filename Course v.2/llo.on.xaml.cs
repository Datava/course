﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Course_v._2
{
    /// <summary>
    /// Логика взаимодействия для llo.xaml
    /// </summary>
    public partial class llo :Window
    {
		private List<Contacts> contact_list = new List<Contacts>();
		private List<int> searchList = new List<int>();
		private MySqlConnection connection;
		public llo() {
			InitializeComponent();
		}

		private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
			if (connection.State == System.Data.ConnectionState.Open)
				connection.Close();
		}

		private async void Window_Loaded(object sender, RoutedEventArgs e) {
			connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["ConnToDB"].ConnectionString);
			await connection.OpenAsync();
			await LoadListBox();
		}

		public bool CheckId(int id) {
			for(int i = 0; i < contact_list.Count; i++) {
				if (contact_list[i].id == id)
					return false;
			}
			return true;
		}

		public int IndexId(int id) {
			for (int i = 0; i < contact_list.Count; i++) {
				if (contact_list[i].id == id)
					return i;
			}
			return -1;
		}

		private async Task LoadListBox() {

			try {
				for (int i = 0; i < contact_list.Count; i++) {
					contact_list[i].messages.Clear();
				}
			} catch (Exception ex) {
				MessageBox.Show(ex.ToString());
			}

			MySqlCommand mySqlCommand = new MySqlCommand($"SELECT * FROM `Messages` WHERE `from_id` = '{Client.id}' OR `to_id` = '{Client.id}'", connection);
			MySqlDataReader reader = null;
			try {
				if (connection.State == System.Data.ConnectionState.Closed) {
					connection.Open();
				}
				reader = (MySqlDataReader)await mySqlCommand.ExecuteReaderAsync();
				while (await reader.ReadAsync()) {
					bool x = true;
					int tmpId;
					if (Convert.ToInt32(reader["from_id"]) == Client.id)
						tmpId = Convert.ToInt32(reader["to_id"]);
					else {
						tmpId = Convert.ToInt32(reader["from_id"]);
						x = false;
					}
					string dt = reader["datetime"].ToString();
					DateTime date;
					DateTime.TryParse(dt, out date);
					string m = Convert.ToString(reader["message"]);
					bool isChecked = Convert.ToBoolean(reader["checked"]);  
					if (CheckId(tmpId)) {
						contact_list.Add(new Contacts(tmpId, connection));
					}

					contact_list[IndexId(tmpId)].messages.Add(new MyMessage(m, date, isChecked, x));
				}
				reader.Close();
			} catch (Exception ex) {
				System.Windows.Forms.MessageBox.Show(ex.Message);
			} finally {
				try {
					contact_list.Sort((a, b) => a.messages[a.messages.Count-1].dateTime.CompareTo(b.messages[b.messages.Count - 1].dateTime));
				} catch (Exception ex) {
					MessageBox.Show(ex.Message);
				} finally {
					contact_list.Reverse();

					reader.Close();
				}
				listBox_users.Items.Clear();
				FillUsers();
			}
		}

		public void FillUsers() {
			listBox_users.Items.Clear();
			for (int i = 0; i < contact_list.Count; i++) {
				UserControls.Users form = new UserControls.Users();
				form.SetName(contact_list[i].login);
				form.SetImage(contact_list[i].image);
				form.SetMessage(contact_list[i].messages[contact_list[i].messages.Count - 1].message);
				listBox_users.Items.Add(form);
			}
		}

		public void FillMessage(int id) {
			listBox_messages.Items.Clear();
			for (int i = 0; i < Client.currentContact.messages.Count; i++) {
				UserControls.For_messages form = new UserControls.For_messages();
				UserControls.TopDate date = new UserControls.TopDate();
				DateTime x1 = new DateTime();
				DateTime x2 = new DateTime();
				if (i != 0) {
					x1 = Client.currentContact.messages[i].dateTime.Date;
					x2 = Client.currentContact.messages[i - 1].dateTime.Date;
				}
				if (i == 0) {
					if (i == 0) {
						date.SetDate(Client.currentContact.messages[i].dateTime.Date);
						listBox_messages.Items.Add(date);
					}
				} else
				if (x1 != x2) {
					date.SetDate(Client.currentContact.messages[i].dateTime.Date);
					listBox_messages.Items.Add(date);
				}
				form.SetMessage(Client.currentContact.messages[i].dateTime, Client.currentContact.messages[i].message, Client.currentContact.messages[i].IsOutbox);
				listBox_messages.Items.Add(form);

				listBox_messages.ScrollIntoView(form);
			}
		}
		
		private async void button_Click(object sender, RoutedEventArgs e) {
			await SendMessage(Client.currentContact.id, textBox_message.Text);

			UserControls.For_messages form = new UserControls.For_messages();
			
			form.SetMessage(DateTime.Now, textBox_message.Text);
			textBox_message.Text = "";
			listBox_messages.Items.Add(form);
			await LoadListBox();
		}

		private async Task SendMessage(int toId, string message) {
			try {
				if (connection.State == System.Data.ConnectionState.Closed) {
					connection.Open();
				}
				string sql = $"INSERT INTO `Messages` (`from_id`, `to_id`, `message`, `datetime`) VALUES ('{Client.id}', '{toId}', '{message}', '{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}')";
				var command = new MySqlCommand(sql, connection);
				await command.ExecuteNonQueryAsync();
			} catch (Exception ex) {
				MessageBox.Show(ex.Message);
			}
		}

		private async void button_search_Click(object sender, RoutedEventArgs e) {
			listBox_users.Visibility = Visibility.Visible;
			listBox_search.Visibility = Visibility.Hidden;
			textBox_search.Text = "";
			await LoadListBox();
		}

		private async Task SearchAsync(string search) {
			MySqlDataReader reader = null;
			if (connection.State == System.Data.ConnectionState.Closed) {
				connection.Open();
			}
			string sql = $"SELECT * FROM Users WHERE login = '{search}' OR name = '{search}' OR surname = '{search}'";
			MySqlCommand command = new MySqlCommand(sql, connection);
			try {
				reader = (MySqlDataReader)await command.ExecuteReaderAsync();
				listBox_search.Items.Clear();
				searchList.Clear();
				while (await reader.ReadAsync()) {
					int id = Convert.ToInt32(reader["id"]);
					if (id != Client.id) {
						searchList.Add(id);
						string login = Convert.ToString(reader["login"]);
						string m = Convert.ToString(reader["name"]) + " " + Convert.ToString(reader["surname"]);

						byte[] img = (byte[])(reader["image"]);

						UserControls.Users form = new UserControls.Users();
						if (img != null) {
							form.SetImage(img);
						}

						form.SetName(login);
						form.SetMessage(m);
						listBox_search.Items.Add(form);
					}
				}
			} catch (Exception ex) {
				System.Windows.Forms.MessageBox.Show(ex.Message);
			} finally {
				reader.Close();
			}    
		}

		private async void textBox_search_TextChanged(object sender, TextChangedEventArgs e) {
			listBox_users.Items.Clear();
			await SearchAsync(textBox_search.Text);
		}

		private void textBox_search_GotFocus(object sender, RoutedEventArgs e) {
			listBox_users.Visibility = Visibility.Hidden;
			listBox_search.Visibility = Visibility.Visible;
		}

		private async void listBox_users_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e) {
			int k = listBox_users.SelectedIndex;
			if (listBox_users.SelectedIndex < 0)
				k = 0;
			Client.currentContact = contact_list[k];
			label_birthday.Content = Client.currentContact.birthday.Date;
			label_name.Content = Client.currentContact.name + " " + Client.currentContact.surname;
			label_email.Content = Client.currentContact.email;
			FillMessage(k);
			await LoadListBox();
		}

		private async void listBox_search_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e) {
			await SendMessage(searchList[listBox_search.SelectedIndex], "Hi!");
		}
	}
}
