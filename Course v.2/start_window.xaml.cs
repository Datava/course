﻿using System.Windows;
using System.Windows.Input;

namespace Course_v._2
{
    /// <summary>
    /// Логика взаимодействия для start_window.xaml
    /// </summary>
    public partial class start_window : Window
    {
		Animation animation = new Animation();
		public static bool isClosing = false;

		public start_window()
        {
            InitializeComponent();
            button_back.Visibility = Visibility.Hidden;
			form_forLogin.Button_signup.Click += Button_signup_Click;
			form_signup.Button_signup.Click += Button_signup_Click1;
			form_forLogin.Button_badMemory.Click += Button_badMemory_Click;
			form_forLogin.Button_login.Click += Button_login_Click;
			form_Verify.Button_confirm.Click += Button_login_Click;
		}

		private void Button_login_Click(object sender, RoutedEventArgs e) {
			if (isClosing)
				this.Close();
		}

		private void Button_badMemory_Click(object sender, RoutedEventArgs e) {
			animation.Swap(form_forLogin, form_recovery);
			animation.Emergence(button_back);
		}

		private void Button_signup_Click1(object sender, RoutedEventArgs e) {
			if (form_signup.CheckInvalid()) {
				animation.Swap(form_signup, form_Verify);
				EmailVerify.SendVerifyEmail();
			}
		}

		private void Button_signup_Click(object sender, RoutedEventArgs e){
			animation.Swap(form_forLogin, form_signup);
			animation.Emergence(button_back);
		}

        private void button_close_Click(object sender, RoutedEventArgs e) {
            Close();
			Connect.connection.CloseAsync();
        }

		private void button_back_Click(object sender, RoutedEventArgs e) {
			if (form_recovery.Visibility == Visibility.Visible) {
				animation.Swap(form_recovery, form_forLogin);
				button_back.Visibility = Visibility.Hidden;
			} else if (form_signup.Visibility == Visibility.Visible) {
				animation.Swap(form_signup, form_forLogin);
				button_back.Visibility = Visibility.Hidden;
			} else if (form_Verify.Visibility == Visibility.Visible)
				animation.Swap(form_Verify, form_signup);
		}

		private void Rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) => DragMove();
	}
}
